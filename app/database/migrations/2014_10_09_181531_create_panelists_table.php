<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePanelistsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('panelists', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('full_name');
			$table->string('origin_school');
			$table->string('degree');
			$table->string('paper_title');
			$table->string('paper_description');
			$table->string('paper_duration');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('panelists');
	}

}
