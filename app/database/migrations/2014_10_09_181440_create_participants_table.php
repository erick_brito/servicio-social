<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateParticipantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('participants', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('email');
			$table->string('origin');
			$table->string('origin_school');
			$table->enum('orgin_type_school', ['publica', 'privada']);
			$table->enum('type_participant', ['Estudiante', 'Profesor', 'Académico', 'Organismo gubernamental', 'Asistente General']);
			$table->string('career');
			$table->string('school_register');
			$table->string('interest_topic');
			$table->enum('complete_social_servide',['Si', 'No']);
			$table->string('place_social_service');
			
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('participants');
	}

}
